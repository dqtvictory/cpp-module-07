#include <iostream>
#include "whatever.hpp"

#define GREEN	"\033[92m"
#define NOCOLOR	"\033[0m"

int	main(void)
{
	std::cout	<< GREEN << "========== int ==========\n" << NOCOLOR;
	{
		int	a = 10, b = 6, c = -15, d = 20;
		std::cout	<< "Swapping a = " << a << " and b = " << b << '\n';
		::swap(&a, &b);
		std::cout	<< "Now a = " << a << " and b = " << b << '\n';

		std::cout	<< "c = " << c << " and d = " << d
					<< " so the smaller between the two is " << ::min(c, d)
					<< " and the greater is " << ::max(c, d) << '\n';
	}

	std::cout	<< GREEN << "\n========== float ==========\n" << NOCOLOR;
	{
		float	a = 1.5f, b = 7.9f, c = -1000.1f, d = 42.4242f;
		std::cout	<< "Swapping a = " << a << " and b = " << b << '\n';
		::swap(&a, &b);
		std::cout	<< "Now a = " << a << " and b = " << b << '\n';

		std::cout	<< "c = " << c << " and d = " << d
					<< " so the smaller between the two is " << ::min(c, d)
					<< " and the greater is " << ::max(c, d) << '\n';
	}

	std::cout	<< GREEN << "\n========== char ==========\n" << NOCOLOR;
	{
		char	a = 'a', b = 'b', c = 'A', d = 'Z';
		std::cout	<< "Swapping a = '" << a << "' and b = '" << b << "'\n";
		::swap(&a, &b);
		std::cout	<< "Now a = '"  << a << "' and b = '" << b << "'\n";

		std::cout	<< "c = '"  << c << "' and d = '" << d
					<< "' so the smaller between the two is '" << ::min(c, d)
					<< "' and the greater is " << ::max(c, d) << '\n';
	}

	std::cout	<< GREEN << "\n========== string ==========\n" << NOCOLOR;
	{
		std::string	a = "Hello", b = "Bonjour", c = "Pony", d = "Unicorn";
		std::cout	<< "Swapping a = '" << a << "' and b = '" << b << "'\n";
		::swap(&a, &b);
		std::cout	<< "Now a = '"  << a << "' and b = '" << b << "'\n";

		std::cout	<< "c = '"  << c << "' and d = '" << d
					<< "' so the smaller between the two is '" << ::min(c, d)
					<< "' and the greater is '" << ::max(c, d) << "'\n";
	}
}
