#include "iter.hpp"

#define GREEN	"\033[92m"
#define NOCOLOR	"\033[0m"

template <typename T>
void	increment(T &num)
{
	++num;
}

void	capitalize(std::string &str)
{
	std::transform(str.begin(), str.end(), str.begin(), ::toupper);
}

int	main(void)
{
	std::cout	<< GREEN << "========== int ==========\n" << NOCOLOR;
	{
		int				arr[] = {2, 4, 6, 8, 10};
		size_t const	size = sizeof(arr) / sizeof(int);

		std::cout << "Before iter: ";
		for (size_t i = 0; i < size; i++)
			std::cout << arr[i] << ' ';
		std::cout << '\n';

		iter(arr, size, increment);

		std::cout << "After iter:  ";
		for (size_t i = 0; i < size; i++)
			std::cout << arr[i] << ' ';
		std::cout << '\n';
	}

	std::cout	<< GREEN << "\n========== char ==========\n" << NOCOLOR;
	{
		char			arr[] = {'a', 'A', '1', 'l', '+'};
		size_t const	size = sizeof(arr) / sizeof(char);

		std::cout << "Before iter: ";
		for (size_t i = 0; i < size; i++)
			std::cout << arr[i] << ' ';
		std::cout << '\n';

		iter(arr, size, increment);

		std::cout << "After iter:  ";
		for (size_t i = 0; i < size; i++)
			std::cout << arr[i] << ' ';
		std::cout << '\n';
	}

	std::cout	<< GREEN << "\n========== float ==========\n" << NOCOLOR;
	{
		float			arr[] = {1.23f, 3.45f, 41.42f, 0.0f, -15.0f};
		size_t const	size = sizeof(arr) / sizeof(float);

		std::cout << "Before iter: ";
		for (size_t i = 0; i < size; i++)
			std::cout << arr[i] << ' ';
		std::cout << '\n';

		iter(arr, size, increment);

		std::cout << "After iter:  ";
		for (size_t i = 0; i < size; i++)
			std::cout << arr[i] << ' ';
		std::cout << '\n';
	}

	std::cout	<< GREEN << "\n========== string ==========\n" << NOCOLOR;
	{
		std::string		arr[] =
		{
			"Popcorn", "unIcORN", "123456", "@!@#&*("
		};
		size_t const	size = sizeof(arr) / sizeof(std::string);

		std::cout << "Before iter: ";
		for (size_t i = 0; i < size; i++)
			std::cout << arr[i] << ' ';
		std::cout << '\n';

		iter(arr, size, capitalize);

		std::cout << "After iter:  ";
		for (size_t i = 0; i < size; i++)
			std::cout << arr[i] << ' ';
		std::cout << '\n';
	}
}
