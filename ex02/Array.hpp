#ifndef ARRAY_HPP
#define ARRAY_HPP

#include <stdexcept>
#include <iostream>
#include <string>

template <typename T>
class Array
{

private:
	unsigned int const	_size;
	T					*_data;

public:

	class IndexOutOfRangeException : public std::exception
	{
	public:
		virtual char const	*what(void) const throw()
		{
			return "Index out of range";
		}
	};

	Array<T>(void) : _size(0), _data(NULL) {}
	Array<T>(unsigned int n) :	_size(n),
								_data(n == 0 ? NULL : new T[n])
	{
		T	*t = new T();
		for (unsigned int i = 0; i < _size; i++)
			_data[i] = *t;
		delete t;
	}

	Array<T>(Array<T> const &arr) :	_size(arr._size),
									_data(arr._size == 0 ? NULL : new T[arr._size])
	{
		// Copy the data of arr into instance
		for (unsigned int i = 0; i < _size; i++)
			_data[i] = arr._data[i];
	}

	T const	&operator[](unsigned int i) const
	{
		if (i >= _size)
			throw IndexOutOfRangeException();
		return (_data[i]);
	}

	T &operator[](unsigned int i)
	{
		if (i >= _size)
			throw IndexOutOfRangeException();
		return (_data[i]);
	}

	Array<T>	&operator=(Array<T> const &arr)
	{
		return ((Array<T> &)arr);
	}

	~Array<T>(void)
	{
		delete[] _data;
	}

	unsigned int	size(void) const
	{
		return (_size);
	}

};

template <typename T>
std::ostream	&operator<<(std::ostream &os, Array<T> const &arr)
{
	os << "{ ";
	for (unsigned int i = 0; i < arr.size(); i++)
	{
		os << arr[i];
		if (i < arr.size() - 1)
			os << ", ";
	}
	os << " }";
	return (os);
}

#endif
