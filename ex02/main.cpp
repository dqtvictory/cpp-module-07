#include <iostream>
#include <cstdlib>
#include <ctime>
#include <Array.hpp>

#define ARR_SIZE	20
#define MAX_VAL		100
#define GREEN		"\033[92m"
#define RED			"\033[91m"
#define NOCOLOR		"\033[0m"

#define MESSAGE(msg)	(std::cout << msg << "... ")
#define MESSAGE_OK		(std::cout << GREEN << "OK\n" << NOCOLOR)
#define MESSAGE_FAIL	(std::cout << RED << "FAIL\n" << NOCOLOR)

int	main(void)
{
	srand(time(NULL));

	MESSAGE("Creating my awesome array");
	Array<int>	numbers(ARR_SIZE);
	MESSAGE_OK;

	MESSAGE("Creating classic mirrored array");
	int	*mirror = new int[ARR_SIZE];
	MESSAGE_OK;

	MESSAGE("Assigning random values to both array");
	for (int i = 0; i < ARR_SIZE; i++)
	{
		const int value = rand() % MAX_VAL;
		numbers[i] = value;
		mirror[i] = value;
	}
	MESSAGE_OK;

	MESSAGE("Testing copy and assignment operators within closed scoped (check with valgrind if desired)");
	{
		Array<int>	tmp = numbers;
		Array<int>	test(tmp);
	}
	MESSAGE_OK;

	// Uncomment the lines below to create errors
	// numbers[ARR_SIZE / 2] = 5;
	// mirror[ARR_SIZE / 2] = 7;

	MESSAGE("Checking if two arrays are identical");
	for (int i = 0; i < ARR_SIZE; i++)
	{
		if (mirror[i] != numbers[i])
		{
			MESSAGE_FAIL;
			std::cerr << "Two arrays don't have identical values!!" << std::endl;
			delete[] mirror;	// Added this line to ensure no memory leaks
			return 1;
		}
	}
	MESSAGE_OK;

	try
	{
		MESSAGE("Testing access to negative index");
		numbers[-2] = 0;
	}
	catch(const std::exception& e)
	{
		MESSAGE_OK;
		std::cerr << "Exception caught: " << e.what() << '\n';
	}

	try
	{
		MESSAGE("Testing access to out-of-bound index");
		numbers[ARR_SIZE] = 0;
	}
	catch(const std::exception& e)
	{
		MESSAGE_OK;
		std::cerr << "Exception caught: " << e.what() << '\n';
	}

	std::cout << "Current array: " << numbers << '\n';
	MESSAGE("Testing modification to array");
	for (int i = 0; i < ARR_SIZE; i++)
	{
		numbers[i] = rand() % MAX_VAL;
	}
	MESSAGE_OK;
	std::cout << "After modif:   " << numbers << '\n';

	delete[] mirror;
}